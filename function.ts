$(function () {
    $('#add').click(handleCreate);
});

function handleCreate() {
    let userList : JQuery = $("#userList");
    let userId :number = userList.find("tr").length;
    let userInput : JQuery = $('#user');
    let user : string = <string>userInput.val();
    userInput.val("");
    let td1 : JQuery = $("<td>" + user + "</td>");
    let td2 : JQuery = $("<td id='edit' class='btn btn-info btn-sm'>Edit</td>");
    td2.click(handleEdit);
    let tr : JQuery = $("<tr id='line_" + userId + "'>");
    tr.append(td1);
    tr.append(td2);
    userList.append(tr);
}

$(function(){
    $('#edit').click(handleEdit);
});

function handleEdit() {
    let userList : JQuery = $("#userList");
    let userId :number = userList.find("tr").length;
    $("tbody tr").last().remove();
    let user: string = prompt("Benutzer ändern", );
    let td1 : JQuery = $("<td>" + user + "</td>");
    let td2 : JQuery = $("<td id='edit' class='btn btn-info btn-sm'>Edit</td>");
    td2.click(handleEdit);
    let tr : JQuery = $("<tr id='line_" + userId + "'>");
    tr.append(td1);
    tr.append(td2);
    userList.append(tr);
}